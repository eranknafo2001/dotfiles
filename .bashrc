#!/bin/bash

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

([ -x "$(command -v emacsclient)" ] && export EDITOR='emacsclient -c -a emacs') ||
([ -x "$(command -v emacs)" ] && export EDITOR='emacs') ||
([ -x "$(command -v nvim)" ] && export EDITOR='nvim') ||
([ -x "$(command -v vim)" ] && export EDITOR='vim')

export LANG=en_US.UTF-8

export PATH=$HOME/.poetry/bin:$HOME/bin:$HOME/.local/bin:/usr/local/bin:$PATH
[ -r "$HOME/.cargo/env" ] && source "$HOME/.cargo/env"

[ -x "$(command -v exa)" ] && alias ls="exa" || alias ls="ls --color"
alias ll='ls -l'
alias la="ll -a"
alias l="ll"

[ -x "$(command -v bat)" ] && alias cat="bat"

[ -x "$(command -v nvim)" ] && alias vim="nvim"
[ -x "$(command -v emacsclient)" ] && alias emacs="emacsclient -c -a emacs"

[ -x "$(command -v starship)" ] && eval "$(starship init bash)"

export MOZ_USE_XINPUT2=1

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
