#!/bin/zsh
# The following lines were added by compinstall

([ -x "$(command -v emacsclient)" ] && export EDITOR='emacsclient -c -a emacs') ||
([ -x "$(command -v emacs)" ] && export EDITOR='emacs') ||
([ -x "$(command -v nvim)" ] && export EDITOR='nvim') ||
([ -x "$(command -v vim)" ] && export EDITOR='vim')


bindkey -v
bindkey "^R" history-incremental-search-backward
bindkey -M vicmd '?' history-incremental-pattern-search-backward

zstyle ':completion:*' list-colors ''
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' menu select
# zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
# zstyle :compinstall filename '/home/eran/.zshrc'

export PATH=$HOME/.poetry/bin:$HOME/bin:$HOME/.local/bin:/usr/local/bin:$HOME/.poetry/bin:$HOME/.yarn/bin:$PATH

[ -r "$HOME/.cargo/env" ] && source "$HOME/.cargo/env"

export LANG=en_US.UTF-8

autoload -Uz compinit
compinit
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

export MOZ_USE_XINPUT2=1

[ -x "$(command -v exa)" ] && alias ls="exa" || alias ls="ls --color"
alias ll='ls -l'
alias la="ll -a"
alias l="ll"

[ -x "$(command -v bat)" ] && alias cat="bat"

[ -x "$(command -v nvim)" ] && alias vim="nvim"

[ -x "$(command -v emacsclient)" ] && alias emacs="emacsclient -c -a emacs"

[ -x "$(command -v starship)" ] && eval "$(starship init zsh)"

[ -x "$(command -v ncmpcpp)" ] && alias ncmpcpp="ncmpcpp -b $HOME/.config/ncmpcpp/bindings" \
                               && alias music="ncmpcpp"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
