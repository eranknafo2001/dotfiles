if type -q nvim
    alias vim="nvim"
end

if type -q bat
    alias cat="bat"
end

if type -q exa
    alias ls="exa"
else
    alias ls="ls --color"
end

alias ll="ls -l"
alias la="ll -a"
alias l="ll"

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
