. ~/.config/fish/aliases.fish

if type -q nvim
    set -x EDITOR "nvim"
else if type -q vim
    set -x EDITOR "vim"
end

set -x PATH $HOME/.poetry/bin $HOME/bin $HOME/.local/bin /usr/local/bin $HOME/.yarn/bin $PATH

if status is-interactive
    fish_vi_key_bindings
    starship init fish | source
end

set FISH_CLIPBOARD_CMD "cat"
