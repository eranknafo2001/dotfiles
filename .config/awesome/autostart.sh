#!/bin/bash

#variety &
nm-applet &
dunst &
udiskie -t &

picom -b &

light-locker --no-lock-on-suspend &
