local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "akinsho/toggleterm.nvim",
  after = "mapx",
  config = function()
    local toggleterm = safe_require("toggleterm")
    if not toggleterm then
      return
    end
    toggleterm.setup({
      open_mapping = false,
    })
    local m = safe_require("mapx")
    if not m then
      return
    end
    m.group({ prefix = "<leader>t" }, "Terminal", function()
      m.nnoremap("t", '<cmd>exe v:count1 . "ToggleTerm"<CR>', "Toggle Term")
      m.nnoremap("a", '<cmd>ToggleTermToggleAll<CR>', "Toggle All Term")
    end)
  end,
})
