local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "rcarriga/nvim-notify",
  after = { "mapx", "telescope" },
  config = function()
    local notify = safe_require("notify")
    if not notify then
      return
    end
    notify.setup()
    vim.notify = notify
    local m = safe_require("mapx")
    if not m then
      return
    end
    m.nnoremap("q", "<cmd>q<CR>", "silent", { ft = "notify" })
    local telescope = safe_require("telescope")
    if telescope then
      m.cmdbang("Messages", function()
        telescope.extensions.notify.notify()
      end, { nargs = 0 })
    end
  end,
})
