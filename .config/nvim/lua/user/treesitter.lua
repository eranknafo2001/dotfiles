local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
    as = "treesitter",
    config = function()
      local parsers = safe_require("nvim-treesitter.parsers")
      if not parsers then
        return
      end
      local orgmode = safe_require("orgmode")
      if orgmode then
        orgmode.setup_ts_grammar()
      end
      local configs = safe_require("nvim-treesitter.configs")
      if not configs then
        return
      end
      configs.setup({
        ensure_installed = {
          "org",
          "markdown",
          "comment",
          "yaml",
          "bash",
          "fish",
          "rust",
          "make",
          "c",
          "cpp",
          "toml",
          "c_sharp",
          "go",
          "dockerfile",
          "lua",
          "python",
          "vim",
          "http",
          "json",
          "jsonc",
          "json5",
          "php",
          "phpdoc",
          "html",
          "css",
          "javascript",
          "typescript",
          "tsx",
          "query",
        },
        highlight = {
          enable = true,
          additional_vim_regex_highlighting = false,
        },
        playground = {
          enable = true,
        },
        textobjects = {
          select = {
            enable = true,
            -- Automatically jump forward to textobj, similar to targets.vim
            lookahead = true,
            keymaps = {
              -- You can use the capture groups defined in textobjects.scm
              ["af"] = "@function.outer",
              ["if"] = "@function.inner",
              ["ac"] = "@class.outer",
              ["ic"] = "@class.inner",
            },
          },
        },
        autotag = {
          enable = true,
        },
        refactor = {
          highlight_definitions = {
            enable = true,
            -- Set to false if you have an `updatetime` of ~100.
            clear_on_cursor_move = true,
          },
          highlight_current_scope = { enable = false },
        },
        rainbow = {
          enable = true,
          -- disable = { "jsx", "cpp" }, list of languages you want to disable the plugin for
          extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
          max_file_lines = nil, -- Do not enable for files with more than n lines, int
          -- colors = {}, -- table of hex strings
          -- termcolors = {} -- table of colour name strings
        },
        matchup = {
          enable = true, -- mandatory, false will disable the whole extension
          disable = {}, -- optional, list of language that will be disabled
        },
      })
      vim.cmd([[
        set foldmethod=expr
        set foldexpr=nvim_treesitter#foldexpr()
      ]])
    end,
  },
  { "nvim-treesitter/playground", after = "treesitter" },
  { "nvim-treesitter/nvim-treesitter-textobjects", after = "treesitter" },
  { "windwp/nvim-ts-autotag", after = "treesitter" },
  { "nvim-treesitter/nvim-treesitter-refactor", after = "treesitter" },
  { "p00f/nvim-ts-rainbow", disable = true, after = "treesitter" },
  { "andymass/vim-matchup", after = "treesitter" },
  {
    "romgrk/nvim-treesitter-context",
    after = "treesitter",
    config = function()
      local treesitter_context = safe_require("treesitter-context")
      if not treesitter_context then
        return
      end
      treesitter_context.setup({
        enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
        throttle = true, -- Throttles plugin updates (may improve performance)
        max_lines = 2, -- How many lines the window should span. Values <= 0 mean no limit.
        patterns = { -- Match patterns for TS nodes. These get wrapped to match at word boundaries.
          -- For all filetypes
          -- Note that setting an entry here replaces all other patterns for this entry.
          -- By setting the 'default' entry below, you can control which nodes you want to
          -- appear in the context window.
          default = {
            "class",
            "function",
            "method",
            -- 'for', -- These won't appear in the context
            -- 'while',
            -- 'if',
            -- 'switch',
            -- 'case',
          },
          -- Example for a specific filetype.
          -- If a pattern is missing, *open a PR* so everyone can benefit.
          --   rust = {
          --       'impl_item',
          --   },
        },
        exact_patterns = {
          -- Example for a specific filetype with Lua patterns
          -- Treat patterns.rust as a Lua pattern (i.e "^impl_item$" will
          -- exactly match "impl_item" only)
          -- rust = true,
        },
      })
    end,
  },
})
