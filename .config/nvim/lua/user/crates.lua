local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "saecki/crates.nvim",
  tag = "v0.2.1",
  requires = { "nvim-lua/plenary.nvim" },
  config = function()
    local crates = safe_require("crates")
    if not crates then
      return
    end

    crates.setup()
  end,
})
