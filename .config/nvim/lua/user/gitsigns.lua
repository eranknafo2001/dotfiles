local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "lewis6991/gitsigns.nvim",
  after = "mapx",
  requires = { "nvim-lua/plenary.nvim" },
  config = function()
    local gitsigns = safe_require("gitsigns")
    if not gitsigns then
      return
    end

    gitsigns.setup({})

    local m = safe_require("mapx")
    if not m then
      return
    end

    m.group({ prefix = "<leader>h" }, "Git Signs", function()
      m.nnoremap("s", "<cmd>Gitsigns stage_hunk<CR>", "Stage hunk")
      m.vnoremap("s", "<cmd>Gitsigns stage_hunk<CR>", "Stage hunk")
      m.nnoremap("u", "<cmd>Gitsigns undo_stage_hunk<CR>", "Unstage hunk")
      m.nnoremap("r", "<cmd>Gitsigns reset_hunk<CR>", "Reset hunk")
      m.vnoremap("r", "<cmd>Gitsigns reset_hunk<CR>", "Reset hunk")
      m.nnoremap("R", "<cmd>Gitsigns reset_buffer<CR>", "Reset buffer")
      m.nnoremap("p", "<cmd>Gitsigns preview_hunk<CR>", "Preview hunk")
      m.nnoremap("b", function()
        gitsigns.blame_line({ full = true })
      end, "Blame line")
      m.nnoremap("S", "<cmd>Gitsigns stage_buffer<CR>", "Stage buffer")
      m.nnoremap("U", "<cmd>Gitsigns reset_buffer_index<CR>", "Reset buffer index")
    end)
  end,
})
