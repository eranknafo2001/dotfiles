local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "folke/which-key.nvim",
  config = function()
    local whichkey = safe_require("which-key")
    if not whichkey then
      return
    end

    whichkey.setup({
      ignore_missing = true,
      plugins = {
        presets = {
          operators = false,
          motions = false,
          text_objects = false,
          nav = false,
          g = false,
        },
      },
    })
  end,
})
