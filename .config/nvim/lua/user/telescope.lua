local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "nvim-telescope/telescope.nvim",
  as = "telescope",
  after = "mapx",
  requires = {
    { "nvim-lua/plenary.nvim" },
    { "nvim-telescope/telescope-fzf-native.nvim", run = "make" },
    { "nvim-telescope/telescope-frecency.nvim", requires = { "tami5/sqlite.lua" } },
    { "LinArcX/telescope-command-palette.nvim" },
    { "gbrlsnchs/telescope-lsp-handlers.nvim" },
    { "nvim-telescope/telescope-symbols.nvim" },
  },
  config = function()
    local telescope = safe_require("telescope")
    if not telescope then
      return
    end

    telescope.load_extension("fzf")
    telescope.load_extension("frecency")
    telescope.load_extension("command_palette")
    telescope.load_extension("lsp_handlers")

    local actions = safe_require("telescope.actions")
    if not actions then
      return
    end

    telescope.setup({
      defaults = {
        mappings = {
          i = {
            ["<C-j>"] = actions.move_selection_next,
            ["<C-k>"] = actions.move_selection_previous,
          },
        },
      },
      pickers = {},
      extensions = {
        fzf = {
          fuzzy = true, -- false will only do exact matching
          override_generic_sorter = true, -- override the generic sorter
          override_file_sorter = true, -- override the file sorter
          case_mode = "smart_case", -- or "ignore_case" or "respect_case"
          -- the default case_mode is "smart_case"
        },
        frecency = {
          show_scores = true,
          show_unindexed = false,
          ignore_patterns = { "*.git/*", "*/tmp/*" },
          disable_devicons = false,
        },
        -- command_palette = {
        -- {"File",
        -- { "entire selection (C-a)", ':call feedkeys("GVgg")' },
        -- { "save current file (C-s)", ':w' },
        -- { "save all files (C-A-s)", ':wa' },
        -- { "quit (C-q)", ':qa' },
        -- { "file browser (C-i)", ":lua require'telescope'.extensions.file_browser.file_browser()", 1 },
        -- { "search word (A-w)", ":lua require('telescope.builtin').live_grep()", 1 },
        -- { "git files (A-f)", ":lua require('telescope.builtin').git_files()", 1 },
        -- { "files (C-f)",     ":lua require('telescope.builtin').find_files()", 1 },
        -- },
        -- {"Help",
        -- { "tips", ":help tips" },
        -- { "cheatsheet", ":help index" },
        -- { "tutorial", ":help tutor" },
        -- { "summary", ":help summary" },
        -- { "quick reference", ":help quickref" },
        -- { "search help(F1)", ":lua require('telescope.builtin').help_tags()", 1 },
        -- },
        -- {"Vim",
        -- { "reload vimrc", ":source $MYVIMRC" },
        -- { 'check health', ":checkhealth" },
        -- { "jumps (Alt-j)", ":lua require('telescope.builtin').jumplist()" },
        -- { "commands", ":lua require('telescope.builtin').commands()" },
        -- { "command history", ":lua require('telescope.builtin').command_history()" },
        -- { "registers (A-e)", ":lua require('telescope.builtin').registers()" },
        -- { "colorshceme", ":lua require('telescope.builtin').colorscheme()", 1 },
        -- { "vim options", ":lua require('telescope.builtin').vim_options()" },
        -- { "keymaps", ":lua require('telescope.builtin').keymaps()" },
        -- { "buffers", ":Telescope buffers" },
        -- { "search history (C-h)", ":lua require('telescope.builtin').search_history()" },
        -- { "paste mode", ':set paste!' },
        -- { 'cursor line', ':set cursorline!' },
        -- { 'cursor column', ':set cursorcolumn!' },
        -- { "spell checker", ':set spell!' },
        -- { "relative number", ':set relativenumber!' },
        -- { "search highlighting (F12)", ':set hlsearch!' },
        -- }
        -- }
      },
    })

    local m = safe_require("mapx")
    if not m then
      return
    end

    local telescope_builtin = safe_require("telescope.builtin")
    if not telescope_builtin then
      return
    end

    m.nnoremap("<C-p>", function()
      if vim.fn.isdirectory(".git") ~= 0 then
        telescope_builtin.git_files()
      else
        telescope_builtin.find_files()
      end
    end)
    m.group({ prefix = "<leader>s" }, "Telescope", function()
      m.nnoremap("F", function()
        telescope_builtin.find_files()
      end, "Find All Files")
      m.nnoremap("f", function()
        if vim.fn.isdirectory(".git") ~= 0 then
          telescope_builtin.git_files()
        else
          telescope_builtin.find_files()
        end
      end, "Find Files")
      m.nnoremap("c", "<cmd>Telescope command_palette<CR>", "Command Palette")
      m.nnoremap("p", "<cmd>Telescope projects<CR>", "Projects")
      m.nnoremap("e", function()
        telescope_builtin.symbols({ sources = { "emoji" } })
      end, "Emoji")
      m.nnoremap("E", function()
        telescope_builtin.symbols()
      end, "Symbols")
      m.nnoremap("a", function()
        telescope.extensions.frecency.frecency()
      end, "Frecency")
      m.nnoremap("s", function()
        telescope_builtin.live_grep()
      end, "Live Grep")
      m.nnoremap("S", function()
        telescope_builtin.live_grep()
      end, "Grep String")
      m.nnoremap("M", function()
        telescope_builtin.man_pages({ sections = { "ALL" } })
      end, "All Man Pages")
      m.nnoremap("m", function()
        telescope_builtin.man_pages()
      end, "Man Pages")
      m.nnoremap("h", function()
        telescope_builtin.help_tags()
      end, "Help Tags")
      m.nnoremap("b", function()
        telescope_builtin.buffers()
      end, "Buffers")
      m.nnoremap("o", function()
        telescope_builtin.oldfiles()
      end, "Old Files")
      m.nnoremap("r", function()
        telescope_builtin.lsp_references()
      end, "LSP: References")
      m.nnoremap("i", function()
        telescope_builtin.treesitter()
      end, "TreeSitter: Items")
      m.group({ prefix = "g" }, "Git", function()
        m.nnoremap("b", function()
          telescope_builtin.git_branches()
        end, "Branches")
      end)
    end)
    m.nnoremap("z=", function()
      telescope_builtin.spell_suggest()
    end, "Telescope: Spelling")
  end,
})
