local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

local function arduino_status()
  local ft = vim.api.nvim_buf_get_option(0, "ft")
  if ft ~= "arduino" then
    return ""
  end
  local status, port = pcall(vim.fn["arduino#GetPort"]())
  if not status then
    return ""
  end
  local line = string.format("[%s]", vim.g.arduino_board)
  if vim.g.arduino_programmer ~= "" then
    line = line .. string.format(" [%s]", vim.g.arduino_programmer)
  end
  if port ~= 0 then
    line = line .. string.format(" (%s:%s)", port, vim.g.arduino_serial_baud)
  end
  return line
end

local function keymap()
  return jim.o.keymap
end

use({
  "nvim-lualine/lualine.nvim",
  as = "lualine",
  after = "gps",
  requires = { "kyazdani42/nvim-web-devicons", opt = true },
  config = function()
    local lualine = safe_require("lualine")
    if not lualine then
      return
    end
    local section_c = { { "filename", path = 1 } }

    local section_x = {
      "encoding",
      "fileformat",
      "filetype",
      {
        arduino_status,
        cond = function()
          return arduino_status() ~= ""
        end,
      },
      {
        keymap,
      },
    }

    local gps = safe_require("nvim-gps")
    if gps then
      table.insert(section_c, { gps.get_location, cond = gps.is_available })
    end
    lualine.setup({
      sections = {
        lualine_c = section_c,
        lualine_x = section_x,
      },
    })
  end,
})
