local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

local use_cmp = true

use({
  {
    "neovim/nvim-lspconfig",
    as = "lsp",
    after = { "mapx", use_cmp and "cmp" or "coq" },
    requires = { "williamboman/nvim-lsp-installer", "onsails/diaglist.nvim" },
    config = function()
      local m = safe_require("mapx")
      if not m then
        return
      end

      local diaglist = safe_require("diaglist")
      if not diaglist then
        return
      end

      m.group("silent", function()
        m.nnoremap("<leader>e", function()
          vim.diagnostic.open_float()
        end, "Open float diagnostic")
        m.nnoremap("[d", function()
          vim.diagnostic.goto_prev()
        end, "which_key_ignore")
        m.nnoremap("]d", function()
          vim.diagnostic.goto_next()
        end, "which_key_ignore")
        m.nnoremap("<leader>q", function()
          diaglist.open_buffer_diagnostics()
        end, "Open diagnostic list")
      end)
      m.group({ prefix = "<leader>c" }, "LSP", function()
        m.nnoremap("R", "<cmd>LspRestart<CR>", "Restart")
      end)

      local on_attach = function(client, bufnr)
        -- Enable completion triggered by <c-x><c-o>
        vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

        m.group("silent", { buffer = bufnr }, function()
          m.nnoremap("gD", function()
            vim.lsp.buf.declaration()
          end, "which_key_ignore")
          m.nnoremap("gd", function()
            vim.lsp.buf.definition()
          end, "which_key_ignore")
          m.nnoremap("K", function()
            vim.lsp.buf.hover()
          end, "which_key_ignore")
          m.nnoremap("gi", function()
            vim.lsp.buf.implementation()
          end, "which_key_ignore")
          m.nnoremap("<C-k>", function()
            vim.lsp.buf.signature_help()
          end, "which_key_ignore")
          m.nnoremap("gr", function()
            vim.lsp.buf.references()
          end, "which_key_ignore")

          m.nnoremap("<leader>f", function()
            vim.lsp.buf.format({ async = true })
          end, "LSP: Format")

          m.group({ prefix = "<leader>c" }, "LSP", function()
            m.nnoremap("f", function()
              vim.lsp.buf.formatting()
            end, "Format")
            m.nnoremap("a", function()
              vim.lsp.buf.code_action()
            end, "Code Action")
            m.vnoremap("a", function()
              vim.lsp.buf.range_code_action()
            end, "Code Action")
            m.nnoremap("r", function()
              vim.lsp.buf.rename()
            end, "Rename")
            m.nnoremap("D", function()
              vim.lsp.buf.type_definition()
            end, "Goto Type Definition")
            m.group({ prefix = "w" }, "Workspace", function()
              m.nnoremap("a", function()
                vim.lsp.buf.add_workspace_folder()
              end, "Add")
              m.nnoremap("r", function()
                vim.lsp.buf.remove_workspace_folder()
              end, "Remove")
              m.nnoremap("l", function()
                print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
              end, "List")
            end)
          end)
        end)
      end
      local lsp_installer = safe_require("nvim-lsp-installer")
      if not lsp_installer then
        return
      end

      local coq = safe_require("coq", true)
      local cmp_lsp = safe_require("cmp_nvim_lsp", true)
      local setup_completion
      if coq then
        setup_completion = function(opts)
          return coq.lsp_ensure_capabilities(opts)
        end
      elseif cmp_lsp then
        setup_completion = function(opts)
          opts.capabilities = cmp_lsp.default_capabilities()
          return opts
        end
      else
        vim.notify("nor coq nor cmp lsp were found")
        setup_completion = function(opts)
          return opts
        end
      end

      local enhance_server_opts = {
        ["sumneko_lua"] = function(opts)
          opts.settings = {
            Lua = {
              runtime = {
                -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                version = "LuaJIT",
              },
              diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = { "vim" },
              },
              workspace = {
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
                checkThirdParty = false,
              },
              -- Do not send telemetry data containing a randomized but unique identifier
              telemetry = {
                enable = false,
              },
            },
          }
        end,
        clangd = function(opts)
          opts.on_attach = function(client, bufnr)
            client.server_capabilities.document_formatting = false
            client.server_capabilities.document_range_formatting = false
            on_attach(client, bufnr)
          end
        end,
        tsserver = function(opts)
          opts.on_attach = function(client, bufnr)
            client.server_capabilities.document_formatting = false
            client.server_capabilities.document_range_formatting = false
            local ts_utils = safe_require("nvim-lsp-ts-utils")
            if ts_utils then
              ts_utils.setup({})
              ts_utils.setup_client(client)
              m.nnoremap("<leader>cs", ":TSLspOrganize<CR>", { buffer = bufnr }, "TS: Organize Imports")
              m.nnoremap("<leader>ci", ":TSLspRenameFile<CR>", { buffer = bufnr }, "TS: Rename File")
              m.nnoremap("<leader>co", ":TSLspImportAll<CR>", { buffer = bufnr }, "TS: Import All")
            end

            on_attach(client, bufnr)
          end
        end,
        rust_analyzer = function(opts)
          opts.on_attach = function(client, bufnr)
            on_attach(client, bufnr)
          end
        end,
      }

      lsp_installer.on_server_ready(function(server)
        -- Specify the default options which we'll use to setup all servers
        local opts = {
          on_attach = on_attach,
        }

        if enhance_server_opts[server.name] then
          -- Enhance the default opts with the server-specific ones
          enhance_server_opts[server.name](opts)
        end

        opts = setup_completion(opts)

        server:setup(opts)
      end)
      local servers = {
        "bashls",
        "pyright",
        "yamlls",
        "sumneko_lua",
        "rust_analyzer",
        "clangd",
        "taplo",
      }

      for _, name in pairs(servers) do
        local server_is_found, server = lsp_installer.get_server(name)
        if server_is_found and not server:is_installed() then
          print("Installing " .. name)
          server:install()
        end
      end

      m.nnoremap("q", "<cmd>q<CR>", "silent", { ft = "qf" })
      m.nnoremap("q", "<cmd>q<CR>", "silent", { ft = "lspinfo" })
    end,
  },
  {
    "jose-elias-alvarez/null-ls.nvim",
    after = "lsp",
    config = function()
      local nullls = require("null-ls")
      nullls.setup({
        sources = {
          nullls.builtins.diagnostics.eslint,
          nullls.builtins.code_actions.eslint,
          nullls.builtins.formatting.prettier,
          nullls.builtins.formatting.stylua,
          nullls.builtins.formatting.clang_format,
          nullls.builtins.formatting.cmake_format,
          nullls.builtins.formatting.fish_indent,
          nullls.builtins.formatting.fixjson,
        },
      })
    end,
  },
  use({ "jose-elias-alvarez/nvim-lsp-ts-utils", after = "lsp", module = "nvim-lsp-ts-utils" }),
  use({ "nvim-lua/lsp_extensions.nvim", after = "lsp" }),
})
if use_cmp then
  use({
    "hrsh7th/nvim-cmp",
    as = "cmp",
    requires = {
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",
      "L3MON4D3/LuaSnip",
      "saadparwaiz1/cmp_luasnip",
      "onsails/lspkind.nvim",
    },
    config = function()
      local cmp = safe_require("cmp")
      if not cmp then
        return
      end
      local lspkind = safe_require("lspkind")
      if not lspkind then
        return
      end
      vim.o.completeopt = "menu,menuone,noinsert"
      local only_if_visible = function(callback)
        return function(fallback)
          if cmp.visible() then
            callback(fallback)
          else
            fallback()
          end
        end
      end
      cmp.setup({
        formatting = {
          format = lspkind.cmp_format({
            mode = "symbol", -- show only symbol annotations
            maxwidth = 50, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)

            -- The function below will be called before any actual modifications from lspkind
            -- so that you can provide more controls on popup customization. (See [#30](https://github.com/onsails/lspkind-nvim/pull/30))
            -- before = function(entry, vim_item)
            --   return vim_item
            -- end,
          }),
        },
        snippet = {
          expand = function(args)
            require("luasnip").lsp_expand(args.body)
          end,
        },
        mapping = {
          ["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "c" }),
          ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "c" }),
          ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
          ["<C-y>"] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
          ["<C-e>"] = cmp.mapping({
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
          }),
          ["<CR>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
          ["<C-p>"] = cmp.mapping(
            only_if_visible(function()
              cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
            end),
            { "i", "s" }
          ),
          ["<C-k>"] = cmp.mapping(
            only_if_visible(function()
              cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
            end),
            { "i", "s" }
          ),
          ["<C-n>"] = cmp.mapping(
            only_if_visible(function()
              cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
            end),
            { "i", "s" }
          ),
          ["<C-j>"] = cmp.mapping(
            only_if_visible(function()
              cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
            end),
            { "i", "s" }
          ),
          ["<Tab>"] = cmp.mapping(
            only_if_visible(function()
              local entry = cmp.get_selected_entry()
              if not entry then
                cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
              else
                cmp.confirm()
              end
            end),
            { "i", "s" }
          ),
        },
        sources = cmp.config.sources({
          { name = "nvim_lsp" },
          -- { name = "luasnip" },
        }, {
          { name = "buffer" },
        }),
      })

      -- -- Set configuration for specific filetype.
      -- cmp.setup.filetype("gitcommit", {
      --   sources = cmp.config.sources({
      --     { name = "cmp_git" }, -- You can specify the `cmp_git` source if you were installed it.
      --   }, {
      --     { name = "buffer" },
      --   }),
      -- })

      cmp.setup.cmdline("/", {
        sources = {
          { name = "buffer" },
        },
      })

      -- cmp.setup.cmdline(":", {
      --   sources = cmp.config.sources({
      --     { name = "path" },
      --   }, {
      --     { name = "cmdline" },
      --   }),
      -- })
    end,
  })
else
  use({
    "ms-jpq/coq_nvim",
    branch = "coq",
    as = "coq",
    after = "mapx",
    requires = { { "ms-jpq/coq.artifacts", branch = "artifacts" }, { "ms-jpq/coq.thirdparty", branch = "3p" } },
    run = ":COQdeps",
    setup = function()
      vim.g.coq_settings = {
        auto_start = "shut-up",
        ["clients.tabnine.enabled"] = true,
        ["keymap.bigger_preview"] = "<A-k>",
        ["keymap.repeat"] = ",",
        ["clients.paths.path_seps"] = { "/" },
      }
    end,
    config = function()
      local m = safe_require("mapx")
      if not m then
        return
      end
      m.imap("<C-j>", [[pumvisible() ? "\<C-n>" : "\<C-j>"]], "expr")
      m.imap("<C-k>", [[pumvisible() ? "\<C-p>" : "\<K-k>"]], "expr")
    end,
  })
end
