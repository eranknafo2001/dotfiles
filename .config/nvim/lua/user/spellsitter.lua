local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "lewis6991/spellsitter.nvim",
  after = "treesitter",
  config = function()
    local spellsitter = safe_require("spellsitter")
    if not spellsitter then
      return
    end
    spellsitter.setup()
  end,
})
