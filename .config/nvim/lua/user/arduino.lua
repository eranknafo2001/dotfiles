local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "stevearc/vim-arduino",
  after = "mapx",
  config = function()
    local m = safe_require("mapx")
    if not m then
      return
    end
    m.group({ ft = "arduino", prefix = "<leader>a" }, "Arduino", function()
      m.nnoremap("a", "<cmd>ArduinoAttach<CR>", "Attach")
      m.nnoremap("m", "<cmd>ArduinoVerify<CR>", "Verify")
      m.nnoremap("u", "<cmd>ArduinoUpload<CR>", "Upload")
      m.nnoremap("d", "<cmd>ArduinoUploadAndSerial<CR>", "Upload and open serial")
      m.nnoremap("s", "<cmd>ArduinoSerial<CR>", "Open Serial")
      m.nnoremap("b", "<cmd>ArduinoChooseBoard<CR>", "Choose Board")
      m.nnoremap("p", "<cmd>ArduinoChooseProgrammer<CR>", "Choose Programmer")
    end)
  end,
})
