local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "nvim-orgmode/orgmode",
  config = function()
    local orgmode = safe_require("orgmode")
    if not orgmode then
      return
    end

    orgmode.setup({
      org_agenda_files = { "~/org/**/*" },
      org_default_notes_file = "~/org/file.org",
    })

    local m = safe_require("mapx")
    if not m then
      return
    end
    m.nnoremap("<leader>o" , "<nop>")
    m.group({ prefix = "<leader>o" }, "Org Mode", function()
      m.nnoremap("a", function()
        orgmode.action("agenda.prompt")
      end, "Agenda Prompt")
      m.nnoremap("c", function()
        orgmode.action("capture.prompt")
      end, "Capture Prompt")
    end)
  end,
})

use({
  "akinsho/org-bullets.nvim",
  config = function()
    local org_bullets = safe_require("org-bullets")
    if not org_bullets then
      return
    end
    org_bullets.setup({
      symbols = { "◉", "○", "✸", "✿" },
    })
  end,
})
