local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "norcalli/nvim-colorizer.lua",
  ft = { "css", "javascript", "html" },
  config = function()
    local colorizer = safe_require("colorizer")
    if not colorizer then
      return
    end
    colorizer.setup({ "css", "javascript", "html" })
  end,
})
