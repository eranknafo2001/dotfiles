local home = vim.fn.expand("$HOME")

vim.cmd[[
  filetype on
]]

vim.o.inccommand = "nosplit"
vim.o.hlsearch = true
vim.o.autoindent = true
vim.o.timeoutlen = 500
vim.o.encoding = "utf-8"
vim.o.scrolloff = 2
vim.o.showmode = false
vim.o.hidden = true
vim.o.wrap = false
vim.o.joinspaces = false
vim.o.exrc = true
vim.o.secure = true
vim.o.splitright = true
vim.o.splitbelow = true
vim.o.number = true
vim.o.relativenumber = true
vim.o.mouse = "a"
vim.o.breakindent = true
vim.o.undodir = home .. "/.vimdid"
vim.o.undofile = true
vim.o.ignorecase = true
--vim.o.cursorline = true
vim.o.smartcase = true
vim.o.incsearch = true
vim.o.gdefault = true
vim.o.updatetime = 250
vim.o.shiftwidth = 2
vim.o.softtabstop = 2
vim.o.tabstop = 2
vim.o.expandtab = true
vim.o.termguicolors = true

vim.o.wildmenu = true
vim.o.wildmode = "list:longest"
vim.opt.wildignore = {
  ".hg",
  ".svn",
  "*~",
  "*.png",
  "*.jpg",
  "*.gif",
  "*.settings",
  "Thumbs.db",
  "*.min.js",
  "*.swp",
  "publish/*",
  "intermediate/*",
  "*.o",
  "*.hi",
  "Zend",
  "vendor",
}
vim.opt.shortmess:append("c")
vim.o.formatoptions = "tcrqnb"
vim.o.guifont = "Hack Nerd Font:h15"
vim.o.backspace = "2"
vim.o.foldenable = false
vim.o.ttyfast = true
vim.o.lazyredraw = true
vim.o.synmaxcol = 500
vim.o.laststatus = 2
vim.opt.diffopt:append({ "iwhite", "algorithm:patience", "indent-heuristic" })
vim.o.showcmd = true
vim.o.listchars = "tab:» ,nbsp:¬,extends:»,precedes:«,trail:•"
vim.o.list = true
vim.api.nvim_set_keymap("", "<Space>", "<Nop>", { noremap = true, silent = true })
vim.g.mapleader = " "
vim.g.maplocalleader = " "

if vim.fn.executable("fish") == 1 then
  vim.o.shell = "fish"
elseif vim.fn.executable("zsh") == 1 then
  vim.o.shell = "zsh"
elseif vim.fn.executable("bash") == 1 then
  vim.o.shell = "bash"
elseif vim.fn.executable("sh") == 1 then
  vim.o.shell = "sh"
end

