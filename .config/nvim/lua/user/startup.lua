local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "startup-nvim/startup.nvim",
  disable = true,
  requires = { "nvim-lua/plenary.nvim" },
  after = "telescope",
  config = function()
    local startup = safe_require("startup")
    if not startup then
      return
    end
    startup.setup({ theme = "dashboard" })
  end,
})
