local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

local icons = {
  error = " ",
  warning = " ",
  info = "",
}

use({
  "akinsho/bufferline.nvim",
  requires = "kyazdani42/nvim-web-devicons",
  tag = "v2.*",
  after = "mapx",
  config = function()
    local bufferline = safe_require("bufferline")
    if not bufferline then
      return
    end
    vim.opt.termguicolors = true
    bufferline.setup({
      options = {
        -- diagnostics = "nvim_lsp",
        -- offsets = {
        --   {
        --     filetype = "NvimTree",
        --     text = "File Explorer",
        --     highlight = "Directory",
        --     text_align = "left",
        --   },
        -- },
        -- custom_areas = {
        --   right = function()
        --     local result = {}
        --     local seve = vim.diagnostic.severity
        --     local error = #vim.diagnostic.get(0, { severity = seve.ERROR })
        --     local warning = #vim.diagnostic.get(0, { severity = seve.WARN })
        --     local info = #vim.diagnostic.get(0, { severity = seve.INFO })
        --     local hint = #vim.diagnostic.get(0, { severity = seve.HINT })

        --     table.insert(result, { text = " iwork " .. error, guifg = "#EC5241" })
        --     if error ~= 0 then
        --       table.insert(result, { text = "  " .. error, guifg = "#EC5241" })
        --     end

        --     if warning ~= 0 then
        --       table.insert(result, { text = "  " .. warning, guifg = "#EFB839" })
        --     end

        --     if hint ~= 0 then
        --       table.insert(result, { text = "  " .. hint, guifg = "#A3BA5E" })
        --     end

        --     if info ~= 0 then
        --       table.insert(result, { text = "  " .. info, guifg = "#7EA9A7" })
        --     end
        --     return result
        --   end,
        -- },
        diagnostics_indicator = function(count, level, diagnostics_dict, context)
          local s = " "
          for e, n in pairs(diagnostics_dict) do
            local sym = icons[e]
            s = s .. n .. sym
          end
          return s
        end,
      },
    })
    local m = safe_require("mapx")
    if not m then
      return
    end
    m.nnoremap("<A-1>", "<Cmd>BufferLineGoToBuffer 1<CR>", "silent")
    m.nnoremap("<A-2>", "<Cmd>BufferLineGoToBuffer 2<CR>", "silent")
    m.nnoremap("<A-3>", "<Cmd>BufferLineGoToBuffer 3<CR>", "silent")
    m.nnoremap("<A-4>", "<Cmd>BufferLineGoToBuffer 4<CR>", "silent")
    m.nnoremap("<A-5>", "<Cmd>BufferLineGoToBuffer 5<CR>", "silent")
    m.nnoremap("<A-6>", "<Cmd>BufferLineGoToBuffer 6<CR>", "silent")
    m.nnoremap("<A-7>", "<Cmd>BufferLineGoToBuffer 7<CR>", "silent")
    m.nnoremap("<A-8>", "<Cmd>BufferLineGoToBuffer 8<CR>", "silent")
    m.nnoremap("<A-9>", "<Cmd>BufferLineGoToBuffer 9<CR>", "silent")
    m.nnoremap("<leader>1", "<Cmd>BufferLineGoToBuffer 1<CR>", "silent", "Goto Buf 1")
    m.nnoremap("<leader>2", "<Cmd>BufferLineGoToBuffer 2<CR>", "silent", "Goto Buf 2")
    m.nnoremap("<leader>3", "<Cmd>BufferLineGoToBuffer 3<CR>", "silent", "Goto Buf 3")
    m.nnoremap("<leader>4", "<Cmd>BufferLineGoToBuffer 4<CR>", "silent", "Goto Buf 4")
    m.nnoremap("<leader>5", "<Cmd>BufferLineGoToBuffer 5<CR>", "silent", "Goto Buf 5")
    m.nnoremap("<leader>6", "<Cmd>BufferLineGoToBuffer 6<CR>", "silent", "Goto Buf 6")
    m.nnoremap("<leader>7", "<Cmd>BufferLineGoToBuffer 7<CR>", "silent", "Goto Buf 7")
    m.nnoremap("<leader>8", "<Cmd>BufferLineGoToBuffer 8<CR>", "silent", "Goto Buf 8")
    m.nnoremap("<leader>9", "<Cmd>BufferLineGoToBuffer 9<CR>", "silent", "Goto Buf 9")
    m.nnoremap("gb", "<Cmd>BufferLineCycleNext<CR>", "silent", "which_key_ignore")
    m.nnoremap("gB", "<Cmd>BufferLineCyclePrev<CR>", "silent", "which_key_ignore")
  end,
})
