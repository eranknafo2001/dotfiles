local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "aserowy/tmux.nvim",
  config = function()
    local tmux = safe_require("tmux")
    if not tmux then return end
    tmux.setup({
      copy_sync = {
        enable = true,
      },
      navigation = {
        enable_default_keybindings = true,
      },
      resize = {
        enable_default_keybindings = true,
      },
    })
  end,
})
