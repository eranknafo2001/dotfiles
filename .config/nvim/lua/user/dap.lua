local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  {
    "mfussenegger/nvim-dap",
    as = "dap",
    disable = true,
    after = "mapx",
    config = function()
      local dap = safe_require("dap")
      if not dap then
        return
      end
      local m = safe_require("mapx")
      if not m then
        return
      end

      vim.fn.sign_define("DapBreakpoint", { text = "🍎", texthl = "", linehl = "", numhl = "" })
      vim.fn.sign_define("DapStopped", { text = "🍏", texthl = "", linehl = "", numhl = "" })

      m.group("silent", function()
        m.nnoremap("<leader>b", function()
          dap.toggle_breakpoint()
        end, "Toggle Breakpoint")
        m.nnoremap("<leader>B", function()
          dap.set_breakpoint(vim.fn.input("Breakpoint condition: "))
        end, "Set Breakpoint with condition")
        m.group({ prefix = "<leader>d" }, "DAP", function()
          m.nnoremap("b", function()
            dap.toggle_breakpoint()
          end, "Toggle Breakpoint")
          m.nnoremap("B", function()
            dap.set_breakpoint(vim.fn.input("Breakpoint condition: "))
          end, "Set Breakpoint with condition")
          m.nnoremap("p", function()
            dap.set_breakpoint(nil, nil, vim.fn.input("Log point message: "))
          end, "Set Logpoint")
          m.nnoremap("c", function()
            dap.continue()
          end, "Continue")
          m.nnoremap("t", function()
            dap.terminate()
            -- print("test")
            local dapui = safe_require("dapui")
            if dapui then
              dapui.close()
            end
          end, "Terminate")
          m.nnoremap("r", function()
            dap.repl.open()
          end, "Repl Open")
        end)
        m.nnoremap("<F5>", function()
          dap.continue()
        end)
        m.nnoremap("<F10>", function()
          dap.step_over()
        end)
        m.nnoremap("<F11>", function()
          dap.step_into()
        end)
        m.nnoremap("<F12>", function()
          dap.step_out()
        end)
      end)
    end,
  },
  {
    "Pocco81/dap-buddy.nvim",
    disable = true,
    after = "dap",
    config = function()
      local Set = require("user.utils").Set
      local dap_install = safe_require("dap-buddy")
      if not dap_install then
        return
      end
      dap_install.setup({
        installation_path = vim.fn.stdpath("data") .. "/dapinstall/",
      })

      local dbg_set = Set(require("dap-install.api.debuggers").get_installed_debuggers())
      local wanted_daps = { "python" }
      for _, dap in ipairs(wanted_daps) do
        if not dbg_set[dap] then
          pcall(vim.cmd, "DIInstall " .. dap)
        end
      end

      local dbg_list = require("dap-install.api.debuggers").get_installed_debuggers()
      local debugger_opts = {}

      for _, debugger in ipairs(dbg_list) do
        if debugger_opts[debugger] ~= nil then
          dap_install.config(debugger, debugger_opts[debugger])
        else
          dap_install.config(debugger, {})
        end
      end
    end,
  },
  {
    "nvim-telescope/telescope-dap.nvim",
    disable= true,
    after = { "dap", "treesitter", "telescope" },
    config = function()
      local telescope = safe_require("telescope")
      if not telescope then
        return
      end
      telescope.load_extension("dap")
    end,
  },
  {
    "rcarriga/nvim-dap-ui",
    disable = true,
    after = { "mapx", "dap" },
    config = function()
      local dapui = safe_require("dapui")
      if not dapui then
        return
      end
      dapui.setup({})
      local dap = safe_require("dap")
      if not dap then
        return
      end
      dap.listeners.after.event_initialized["dapui_config"] = function()
        dapui.open()
      end
      dap.listeners.before.event_terminated["dapui_config"] = function()
        dapui.close()
      end
      dap.listeners.before.event_exited["dapui_config"] = function()
        dapui.close()
      end
      local m = safe_require("mapx")
      if not m then
        return
      end
      m.nnoremap("<M-k>", function()
        require("dapui").eval()
      end)
      m.vnoremap("<M-k>", function()
        require("dapui").eval()
      end)
    end,
  },
})
