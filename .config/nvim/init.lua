require("user.utils")
safe_require('impatient')
local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({ "alec-gibson/nvim-tetris", cmd = "Tetris" })
-- use { 'chaoren/vim-wordmotion' }
use({ "romainl/vim-cool" })
use({ "tpope/vim-eunuch" })
-- use({ "yamatsum/nvim-cursorline" })
use({
  "~/projects/php-tmpl.nvim",
  disable = true,
  config = function()
    require("php-tmpl").setup()
  end,
})
use("lewis6991/impatient.nvim")

require("user.base")
require("user.packer")
require("user.autopairs")
require("user.treesitter")
require("user.markdown")
require("user.gitsigns")
require("user.kommentary")
require("user.lualine")
require("user.mappings")
require("user.neogit")
require("user.surround")
require("user.telescope")
require("user.theme")
require("user.which-key")
require("user.orgmode")
require("user.colorizer")
require("user.iron")
require("user.undotree")
require("user.headlines")
require("user.startuptime")
require("user.explorer")
require("user.tmux")
require("user.wilder")
require("user.project")
require("user.startup")
require("user.lsp")
require("user.diffview")
require("user.indent-line")
require("user.bufferline")
require("user.neoclip")
require("user.harpoon")
require("user.gps")
require("user.spellsitter")
require("user.dap")
require("user.notify")
require("user.reload")
require("user.terminal")
require("user.terminal-color")
require("user.arduino")
require("user.rest")
require("user.crates")

-- packer.compile()
