from typing import Callable, List
from subprocess import check_output
from libqtile.bar import Bar
from libqtile.config import Screen


class ScreenManager:
    def __init__(
        self,
        create_main_bar: Callable[[], Bar],
        create_secodary_bar: Callable[[], Bar],
    ) -> None:
        self._create_main_bar = create_main_bar
        self._create_secodary_bar = create_secodary_bar

    @staticmethod
    def _get_screens_names() -> List[str]:
        return [
            l.split()[-1]
            for l in check_output(["xrandr", "--listmonitors"])
            .decode("utf-8")
            .splitlines()
            if ": +" in l
        ]

    @classmethod
    def _get_screen_count(cls) -> int:
        return len(cls._get_screens_names())

    def get_screens(self) -> List[Screen]:
        return [
            Screen(
                top=self._create_main_bar() if i == 0 else self._create_secodary_bar()
            )
            for i in range(self._get_screen_count())
        ]
