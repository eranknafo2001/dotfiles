from itertools import chain
from keyboard_widigt import KeyboardLayout
from colors import ColorManager
import os

from libqtile.bar import Bar
from libqtile.widget.battery import Battery
from libqtile.widget.clock import Clock
from libqtile.widget.currentlayout import CurrentLayout, CurrentLayoutIcon
from libqtile.widget.groupbox import GroupBox
from libqtile.widget.memory import Memory
from libqtile.widget.net import Net
from libqtile.widget.notify import Notify
from libqtile.widget.spacer import Spacer
from libqtile.widget.spacer import Spacer
from libqtile.widget.systray import Systray
from libqtile.widget.textbox import TextBox
from libqtile.widget.volume import Volume
from libqtile.widget.windowname import WindowName


class BarManager:
    def __init__(
        self,
        colors: ColorManager,
        seperator_size: int,
        group_size: int,
        bar_size: int,
        text_size: int,
        network_interface: str,
    ):
        self._seperator_size = seperator_size
        self._group_size = group_size
        self._colors = colors
        self._bar_size = bar_size
        self._text_size = text_size
        self._network_interface = network_interface

    def _create_seperator(self) -> TextBox:
        return TextBox(text="|", padding=0, fontsize=self._seperator_size)

    @staticmethod
    def _get_buttery_names():
        power_path = "/sys/class/power_supply"
        return (
            [int(item[3:]) for item in os.listdir(power_path) if "BAT" in item]
            if os.path.exists(power_path)
            else []
        )

    def _get_butterys(self):
        return list(
            chain.from_iterable(
                (
                    (
                        Battery(
                            format=" {char} {percent:2.0%}",
                            charge_char="⚡",
                            discharge_char="🔋",
                            full_char="⚡",
                            unknown_char="⚡",
                            empty_char="⁉️ ",
                            update_interval=2,
                            show_short_text=False,
                            low_percentage=0.2,
                            notify_below=20,
                            default_text="",
                            battery=bat,
                        ),
                        self._create_seperator(),
                    )
                    for bat in self._get_buttery_names()
                )
            )
        )

    def _create_group_box(self):
        return GroupBox(
            font="Hack Nerd Font",
            fontsize=self._group_size,
            margin_y=4,
            margin_x=0,
            padding_y=5,
            padding_x=3,
            borderwidth=2,
            active=self._colors.text,
            inactive=self._colors.text,
            rounded=False,
            highlight_method="line",
            highlight_color=self._colors.selected_background,
            this_current_screen_border=self._colors.pramery,
            this_screen_border=self._colors.secendery,
            other_current_screen_border=self._colors.pramery,
            other_screen_border=self._colors.secendery,
            foreground=self._colors.text,
            background=self._colors.pramiery_background,
            use_mouse_wheel=False,
            disable_drag=True,
        )

    def create_main_bar(self):
        return Bar(
            [
                self._create_group_box(),
                Spacer(),
                WindowName(foreground=self._colors.pramery),
                Spacer(),
                Notify(default_timeout=5),
                self._create_seperator(),
                Net(interface=self._network_interface, format="{down} ↓↑ {up}"),
                self._create_seperator(),
                TextBox(text=" 🖬", fontsize=14),
                Memory(),
                self._create_seperator(),
                TextBox(text=" Vol:"),
                Volume(),
                self._create_seperator(),
                CurrentLayoutIcon(scale=0.7),
                CurrentLayout(),
                self._create_seperator(),
                *self._get_butterys(),
                Systray(icon_size=16),
                self._create_seperator(),
                KeyboardLayout(update_interval=1, format="{layout}"),
                self._create_seperator(),
                Clock(format="%A, %B %d - %H:%M "),
            ],
            self._bar_size,
        )

    def create_secodary_bar(self):
        return Bar(
            [
                self._create_group_box(),
                Spacer(),
                WindowName(foreground=self._colors.pramery),
                Spacer(),
                Clock(format="%A, %B %d - %H:%M "),
            ],
            self._bar_size,
        )

    def create_widget_defaults(self):
        return dict(
            font="Ubuntu Mono",
            # font="Hack Nerd Font",
            fontsize=self._text_size,
            padding=2,
            background=self._colors.pramiery_background,
        )
