import os
import subprocess
from typing import List

from libqtile import hook, layout
from libqtile.config import Group, Match

from bars import BarManager
from colors import ColorManager
from keys import KeysManager
from local_config import (BAR_SIZE, GROUP_SIZE, NETWORK_INTERFACE,
                          SEPERATOR_SIZE, TEXT_SIZE)
from screens import ScreenManager

colors = ColorManager(
    pramery="#4596ff",
    secendery="#434758",
    pramiery_background="#24272b",
    secondary_background="#24272b",
    selected_background="#323942",
    text="#ffffff",
)

bar_manager = BarManager(
    colors, SEPERATOR_SIZE, GROUP_SIZE, BAR_SIZE, TEXT_SIZE, NETWORK_INTERFACE
)

keys_manager = KeysManager("mod4", "mod1", "control", "shift", "alacritty")

screen_manager = ScreenManager(
    bar_manager.create_main_bar, bar_manager.create_secodary_bar
)

keys = keys_manager.create_keys()

# Drag floating layouts.
mouse = keys_manager.create_mouse_click()

groups_names = [
    ("1", {"layout": "max", "spawn": "firefox"}),
    ("2", {"layout": "max", "spawn": "alacritty"}),
    ("3", {"layout": "max", "spawn": "alacritty"}),
    ("4", {"layout": "max", "spawn": "gmpc"}),
    ("5", {"layout": "max", "spawn": "walc"}),
    ("6", {"layout": "max"}),
    ("7", {"layout": "max"}),
    ("8", {"layout": "max"}),
]
groups = [Group(name, **kwargs) for name, kwargs in groups_names]

for index, group in enumerate(groups, 1):
    keys.extend(keys_manager.create_keys_for_group(group, str(index)))

layout_theme = {
    "border_width": 2,
    "margin": 8,
    "border_focus": "e1acff",
    "border_normal": "1D2330",
}

layouts = [
    layout.Max(**layout_theme),
    layout.MonadTall(**layout_theme),
    # layout.Columns(**layout_theme),
    layout.Bsp(**layout_theme),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = bar_manager.create_widget_defaults()
extension_defaults = widget_defaults.copy()

screens = screen_manager.get_screens()


dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"


@hook.subscribe.startup_once
def _start_once():
    home = os.path.expanduser("~/.config/qtile/autostart.sh")
    subprocess.call([home])


# to make dum java applications work
wmname = "LG3D"
