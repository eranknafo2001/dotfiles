from libqtile.config import Click, Drag, Group, Key
from libqtile.lazy import lazy


class KeysManager:
    def __init__(self, mod: str, alt: str, ctrl: str, shift: str, terminal: str):
        self._mod = mod
        self._alt = alt
        self._ctrl = ctrl
        self._shift = shift
        self._terminal = terminal

    def create_keys(self):
        return [
            Key([self._mod, self._shift, self._ctrl], "q", lazy.shutdown()),
            Key([self._mod, self._ctrl], "q", lazy.restart()),
            Key([self._mod, self._shift], "c", lazy.window.kill()),
            # Switch between windows
            Key([self._mod], "h", lazy.layout.left(), desc="Move focus to left"),
            Key([self._mod], "l", lazy.layout.right(), desc="Move focus to right"),
            Key([self._mod], "j", lazy.layout.down(), desc="Move focus down"),
            Key([self._mod], "k", lazy.layout.up(), desc="Move focus up"),
            Key(
                [self._alt],
                "Tab",
                lazy.layout.next(),
                desc="Move window focus to other window",
            ),
            Key(
                [self._mod],
                "Tab",
                lazy.next_layout(),
                desc="Move window focus to other window",
            ),
            # Move windows between left/right columns or move up/down in current stack.
            # Moving out of range in Columns layout will create new column.
            Key(
                [self._mod, self._shift],
                "h",
                lazy.layout.shuffle_left(),
                desc="Move window to the left",
            ),
            Key(
                [self._mod, self._shift],
                "l",
                lazy.layout.shuffle_right(),
                desc="Move window to the right",
            ),
            Key(
                [self._mod, self._shift],
                "j",
                lazy.layout.shuffle_down(),
                desc="Move window down",
            ),
            Key(
                [self._mod, self._shift],
                "k",
                lazy.layout.shuffle_up(),
                desc="Move window up",
            ),
            # Grow windows. If current window is on the edge of screen and direction
            # will be to screen edge - window would shrink.
            Key(
                [self._mod, self._ctrl],
                "h",
                lazy.layout.grow_left(),
                desc="Grow window to the left",
            ),
            Key(
                [self._mod, self._ctrl],
                "l",
                lazy.layout.grow_right(),
                desc="Grow window to the right",
            ),
            Key(
                [self._mod, self._ctrl],
                "j",
                lazy.layout.grow_down(),
                desc="Grow window down",
            ),
            Key(
                [self._mod, self._ctrl], "k", lazy.layout.grow_up(), desc="Grow window up"
            ),
            Key(
                [self._mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"
            ),
            # Toggle between split and unsplit sides of stack.
            # Split = all windows displayed
            # Unsplit = 1 window displayed, like Max layout, but still with
            # multiple stack panes
            Key(
                [self._mod, self._shift],
                "Return",
                lazy.layout.toggle_split(),
                desc="Toggle between split and unsplit sides of stack",
            ),
            Key([self._mod], "Return", lazy.spawn(self._terminal), desc="Launch terminal"),
            Key([self._mod], "b", lazy.spawn("firefox"), desc="Launch firefox"),
            Key([self._mod], "Escape", lazy.spawn("light-locker-command -l"), desc="Run lock"),
            Key(
                [self._mod],
                "r",
                lazy.spawn("rofi -show drun"),
                desc="Spawn a command using a prompt widget",
            ),
            Key(
                [self._mod, self._alt, self._shift],
                "r",
                lazy.spawn(
                    "bash -c 'SUDO_ASKPASS=~/.local/bin/askpass-rofi rofi -show run -run-command \"sudo -A {cmd}\"'"
                ),
                desc="Spawn a command using a prompt widget",
            ),
            Key(
                [self._mod, self._alt],
                "r",
                lazy.spawn("rofi -show run"),
                desc="Spawn a command using a prompt widget",
            ),
            Key(
                [],
                "XF86AudioRaiseVolume",
                lazy.spawn(
                    'bash -c "pamixer -u && pamixer -i 5"'
                ),
            ),
            Key(
                [],
                "XF86AudioLowerVolume",
                lazy.spawn(
                    'bash -c "pamixer -u && pamixer -d 5"'
                ),
            ),
            Key([], "XF86AudioMute", lazy.spawn("pamixer -t")),
            Key([], "XF86AudioPlay", lazy.spawn("mpc toggle")),
            Key([], "XF86AudioStop", lazy.spawn("mpc pause")),
            Key([], "XF86MonBrightnessUp", lazy.spawn("sudo light -A 1")),
            Key([], "XF86MonBrightnessDown", lazy.spawn("sudo light -U 1")),
            Key([self._mod, self._ctrl], "Right", lazy.screen.next_group()),
            Key([self._mod, self._ctrl], "Left", lazy.screen.prev_group()),
            Key([self._mod], "period", lazy.next_screen()),
            Key([self._mod], "comma", lazy.prev_screen()),
        ]

    def create_keys_for_group(self, group: Group, key: str):
        return [
            Key(
                [self._mod],
                key,
                lazy.group[group.name].toscreen(),
                desc="Switch to group {}".format(group.name),
            ),
            Key(
                [self._mod, self._shift],
                key,
                lazy.window.togroup(group.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(group.name),
            ),
            Key(
                [self._mod, self._ctrl, self._shift],
                key,
                lazy.window.togroup(group.name),
                desc="Switch to & move focused window to group {}".format(group.name),
            ),
        ]

    def create_mouse_click(self):
        return [
            Drag(
                [self._mod],
                "Button1",
                lazy.window.set_position_floating(),
                start=lazy.window.get_position(),
            ),
            Drag(
                [self._mod],
                "Button3",
                lazy.window.set_size_floating(),
                start=lazy.window.get_size(),
            ),
            Click([self._mod], "Button2", lazy.window.toggle_floating()),
        ]
